package xyz.oeste.geob4b.implementacion;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import xyz.oeste.geob4b.serviciointerface.ServicioInterface;
import xyz.oeste.geob4b.util.Mail;

@Service
public class ImplementacionServicio implements ServicioInterface{

	@Override
	public String envioComentario(String name, String last_name, String company_name, String e_mail, String phone,
			String message) {
		JsonObject respuesta = new JsonObject();
		try {
			new Mail().sendCorreo(name, last_name, company_name, e_mail, phone, message);
			respuesta.addProperty("message", "success");
		} catch (Exception e) {
			respuesta.addProperty("message", "error");
		}
		return new Gson().toJson(respuesta);
	}

}
