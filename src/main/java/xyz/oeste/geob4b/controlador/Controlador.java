package xyz.oeste.geob4b.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import xyz.oeste.geob4b.serviciointerface.ServicioInterface;

@RestController
@RequestMapping("/api")
public class Controlador {
	@Autowired
	ServicioInterface service;
	
	@RequestMapping(value="/contact", method=RequestMethod.POST, produces="application/json")
    public String envioComentario(String name, String last_name, String company_name, String e_mail, String phone, String message) {
        return service.envioComentario(name, last_name, company_name, e_mail, phone, message);
    }

}
