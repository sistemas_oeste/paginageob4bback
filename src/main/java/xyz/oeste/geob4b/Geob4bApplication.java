package xyz.oeste.geob4b;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Geob4bApplication {

	public static void main(String[] args) {
		SpringApplication.run(Geob4bApplication.class, args);
	}

}
