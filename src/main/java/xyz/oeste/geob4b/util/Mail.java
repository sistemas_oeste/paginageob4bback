package xyz.oeste.geob4b.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Mail {

	public void sendCorreo(String name, String last_name, String company_name, String e_mail, String phone,
			String message) throws MessagingException {
		String emailend = "web@geob4b.com";
		String password = "sQeIbP)Cgz0;";
		String host = "mail.geob4b.com";
		Properties props = System.getProperties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.user", emailend);
		props.put("mail.smtp.clave", password);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "false");
		props.put("mail.smtp.port", "587");
		Session session = Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(emailend, password);
			}
		});
		session.setDebug(true);
		BodyPart body = new MimeBodyPart();
		body.setText(cuerpo_mail(name, last_name, company_name, e_mail, phone, message).toString());
		MimeMultipart multiParte = new MimeMultipart();
		multiParte.addBodyPart(body);
		MimeMessage message1 = new MimeMessage(session);
		try {
			message1.setFrom(new InternetAddress(emailend));
			message1.addRecipients(Message.RecipientType.TO, "info@geob4b.com");
			//message1.addRecipients(Message.RecipientType.TO, "ljimenez@oeste.xyz");
			message1.setSubject(asuto_mail(name, last_name, company_name).toString());
			message1.setContent(multiParte);
			Transport transport = session.getTransport("smtp");
			transport.connect(host, emailend);
			transport.sendMessage(message1, message1.getAllRecipients());
			transport.close();
		} catch (MessagingException me) {
			me.printStackTrace();
		}
	}

	protected StringBuilder cuerpo_mail(String name, String last_name, String company_name, String e_mail, String phone,
			String message) {
		StringBuilder cuerpo_mail = new StringBuilder();
		cuerpo_mail.append("Nombre: ");
		cuerpo_mail.append(name);
		cuerpo_mail.append(" ");
		cuerpo_mail.append(last_name);
		cuerpo_mail.append("\n");
		cuerpo_mail.append("Empresa: ");
		cuerpo_mail.append(company_name);
		cuerpo_mail.append("\n");
		cuerpo_mail.append("Correo: ");
		cuerpo_mail.append(e_mail);
		cuerpo_mail.append("\n");
		cuerpo_mail.append("Telefono: ");
		cuerpo_mail.append(phone);
		cuerpo_mail.append("\n");
		cuerpo_mail.append("Mensaje: ");
		cuerpo_mail.append(message);
		return cuerpo_mail;
	}

	protected StringBuilder asuto_mail(String name, String last_name, String company_name) {
		StringBuilder asunto_mail = new StringBuilder();
		asunto_mail.append("Solicitud de Base de Datos geoB4B ");
		asunto_mail.append(name);
		asunto_mail.append(" ");
		asunto_mail.append(last_name);
		asunto_mail.append(" ");
		asunto_mail.append(company_name);
		return asunto_mail;
	}

}
